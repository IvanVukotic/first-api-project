import psycopg2
from psycopg2.extras import RealDictCursor # add names of fiels to cursor
from random import randrange
from fastapi import Body, FastAPI, HTTPException, Response, status
from pydantic import BaseModel  # Schema
from typing import Optional  # Optional fields
try:
    db_connection = psycopg2.connect(
        host='localhost',
        database='Reddit Clone',
        user='postgres',
        password="findMepodatke558#",
        cursor_factory=RealDictCursor
    )
    cursor = db_connection.cursor()
    print('Database Connection: successful')
except Exception as e:
    print('Database connection: failed')
    print("Error:", e.json)





class BlogPost(BaseModel):  # Pydantic schema for POST Body validation
    title: str
    content: str
    published: bool = True
    rating: Optional[int] = None


##### DATA #####: Array of Blog Posts (Local list)
my_posts = [ 
    {"id": 1, "title": "Welcome to our blog", "content": "This is the beginning..."},
    {"id": 2, "title": "Top 10 best activities in Luxembourg", "content": "Our list of..."}
]

# find and return the post with the a given i
def find_post(given_id):
    for post in my_posts:
        if post["id"] == given_id:
            return post

# Find and return the index of a specific post
def find_post_index(given_id):
    for index, post in enumerate(my_posts):
        if post["id"] == given_id:
            return index

        
###### FastAPI instance name ######
app = FastAPI()  



#######################################
#### "Path Operations" or "Routes"  ###
#######################################

# GET / :
@app.get("/")  # decorator
def hello():  # function
    return {"message": "Hello from my first API"}  # response



# GET /posts ***GET ALL BLOGPOSTS***
@app.get("/posts")
def get_posts():
    # EXecuting the SQL query
    cursor.execute("SELECT * FROM POSTS ORDER BY id")
    # Retrieve all the posts (lists)
    database_posts = cursor.fetchall()
    return {"data": database_posts}



# POST /posts  ***CREATE NEW BLOGPOST***
@app.post("/posts", status_code=status.HTTP_201_CREATED)   # POST /posts endpoint
def create_posts(new_post: BlogPost, response: Response):  # Use the pydantic schema class
                             # Convert the pydantic model to a dictionary                                                 # get last id in the list and increment it by 1
    cursor.execute(f"INSERT INTO posts (title, content, published) VALUES (%s ,%s, %s) RETURNING *;",(new_post.title, new_post.content, new_post.published) )
    post_dict = cursor.fetchall()
    db_connection.commit()
    # response.status_code = 201                           # Alternative solution to change the HTTP code
    return {"data": post_dict}    



#GET /posts/trending  ***GET LATEST BLOGPOST***


#GET /posts/{id_param}  ***GET POST WITH ID***
@app.get('/posts/{id_param}')                              # {id_param}  is the path parameter
def get_post(id_param: int, response: Response): 
    cursor.execute("SELECT * FROM POSTS WHERE id = %s;", (id_param,))# id_param must be an integer                                       
    corresponding_post = cursor.fetchone()               # use the function with converted URI
    if not corresponding_post:                             # If No corresponding post found throw an exception to the consumer
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )
    return {"data": corresponding_post}   



@app.delete('/posts/{id_param}')
def delete_post(id_param: int):
    cursor.execute("DELETE FROM POSTS WHERE id = %s RETURNING *;", (id_param,))
    post_dig = cursor.fetchone()
    db_connection.commit()
    if not post_dig:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )

    # 2. Remove element from array
    return {"data": Response(status_code=status.HTTP_204_NO_CONTENT)}




#PUT /posts/{id_param}  ***REPLACE POST WITH ID***
@app.put('/posts/{id_param}')
def replace_post(id_param: int, updated_post: BlogPost):
    cursor.execute("UPDATE posts SET title = %s, content = %s, published = %s  WHERE id = %s RETURNING *;", (updated_post.title, updated_post.content, updated_post.published, id_param))
    corresponding_index = cursor.fetchone()
    db_connection.commit()
    if not corresponding_index:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )
    # 3. Pydantic class to Dict
    return {"data": corresponding_index}
