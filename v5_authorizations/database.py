from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from config import settings
#this goes as typeofdatabase://user on your system:password@where is the database:port/name of database
DATABASE_URL=f'postgresql://{settings.database_user}:{settings.database_password}@{settings.database_host}:{settings.database_port}/{settings.database_name}'

#make an engine which will later write on the database
database_engine = create_engine(DATABASE_URL)

# make a session which will hold the communication between the code and the database
SessionTemplate = sessionmaker(autocommit=False, autoflush=False, bind=database_engine)

#just to make that the database always connects
def get_db():
    db = SessionTemplate()
    try:
        yield db
    finally:
        db.close()
        