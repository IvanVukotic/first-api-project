import pytest
from fastapi.testclient import TestClient
from main import app
from models import Base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from database import get_db
#this goes as typeofdatabase://user on your system:password@where is the database:port/name of database
DATABASE_URL='postgresql://postgres:findMepodatke558#@localhost:5432/sqlalchemy_test'

#make an engine which will later write on the database
database_engine = create_engine(DATABASE_URL)

# make a session which will hold the communication between the code and the database
TestingSessionTemplate= sessionmaker(autocommit=False, autoflush=False, bind=database_engine)

#just to make that the database always connects
def override_get_db():
    db = TestingSessionTemplate()
    try:
        yield db
    finally:
        db.close()
        
@pytest.fixture()
def session():
    #Clean DB by deleting previous tables
    Base.metadata.drop_all(bind=database_engine)
    #Create tables
    Base.metadata.create_all(bind=database_engine)
    app.dependency_overrides[get_db] =override_get_db
    
@pytest.fixture()# Since we made authorization this makes our user for us without it
def create_user(client):
    user_credentials =  {"email":"luckyfrree@domain.lu", "password" : "5678"}
    #post request to create new user
    res = client.post("/users/", json=user_credentials)
    #response json inlcuding only id email and created at
    new_user = res.json()
    #adding password
    new_user["password"] = user_credentials["password"]
    #returning new user with everything+ plain text password
    return new_user
    
    
    
    
@pytest.fixture()
def client(session):
    yield TestClient(app)
    
@pytest.fixture
def user_token(create_user, client):
    res = client.post("/auth/", data={"username": create_user['email'], "password": create_user['password']})
    print(res.json().get("access_token"))
    return res.json().get("access_token")

@pytest.fixture
def authorized_client(client, user_token):
    client.headers = {**client.headers, "Authorization": f"Bearer {user_token}"}
    print(client)
    return client

@pytest.fixture()
def create_post(client):
    user_post = {"title": "we are trying our best",
        "content": "Free from desire modern senses",
        "published": true,
        "writer_id": 1}
    res = client.post("/posts/", json=user_post)
    new_post = res.json()
    return new_post