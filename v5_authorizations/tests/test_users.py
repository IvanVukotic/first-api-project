import pytest
from fastapi.testclient import TestClient
from jose import jwt
from utilities.jwt_manager import ALGORITHM, SERVER_KEY
import json

SERVER_KEY = "9faeef750a05842df9ce1240b334863ef0dde90525b37ca4e6cd426d7985d210"
ALGORITHM = "HS256"

def test_create_user(client):
    res = client.post('/users', 
                      json = {"email":"freeakingcrazy@domain.lu", "password":"1234"})
    print(res.json())
    assert res.json().get("email") == "freeakingcrazy@domain.lu"
    assert res.status_code == 201

# def test_create_posts(client):
#     res = client.post('/posts', 
#                       {"title": "we are trying our best",
        # "content": "Free from desire modern senses",
        # "published": true,
        # "writer_id": 1})
#     print(res.json())
#     assert res.json().get("email") == "freeakingcrazy@domain.lu"
#     assert res.status_code == 201

def test_login_user(create_user, client):
    result = client.post('/auth', data = {"username": create_user['email'], "password": create_user['password']})
    assert result.status_code == 202
    assert result.json().get("token_type") == "bearer"
    #verify the token data
    payload = jwt.decode(result.json().get("access_token"), SERVER_KEY, algorithms=[ALGORITHM])
    id = payload.get("user_id")
    assert id == create_user["id"]
    
def test_me(create_user, authorized_client):
    res = authorized_client.get('/users/me')
    assert res.status_code == 200
    assert res.json().get('id') == create_user["id"]
    assert res.json().get("email") == create_user['email']
    
@pytest.mark.parametrize("email, password, status_code", 
                         [("freeakingcrazy@domain.lu", None, 422), 
                          (None, "1234", 422), 
                          ("fdfsagf@domain.lu", "1234", 401),
                          ("freeakingcrazy@domain.lu","45868",401),
                          ("hfdgas@domain.lu", "78636",401)])
def test_incorect_credentials(create_user, client, email, password, status_code):
        res = client.post("/auth", data={"username": email, "password": password})
        assert res.status_code == status_code
    
def test_me_incorrect(create_user, client):
    res = client.get('/users/me')
    assert res.status_code == 401  


@pytest.mark.parametrize("title, content, published, writer_id, status_code", [("whatever", "whoever reads this", None, 2, 422), ("whatever", "whoever reads this", True, None ,422)])
def test_create_post(authorized_client, title, content, published, writer_id, status_code):
    res = authorized_client.post("/posts", json={"title":title, "content": content, "published":published, "writer_id":writer_id})
    assert res.status_code == status_code



@pytest.mark.parametrize("uri_id, status_code", [(15, 404), (20, 404)])
def test_get_one_post(uri_id, authorized_client, status_code):
    uri = str(uri_id)
    res = authorized_client.get('/users/'+ uri)
    assert res.status_code == status_code
    
def test_get_all( client):
    res = client.get('/posts')
    assert res.status_code == 401
    
@pytest.mark.parametrize("uri_id, title, content, published, writer_id, status_code", [(15,"whatever", "whoever reads this", True, 2, 404), 
                                                                                       (1,"whatever", "whoever reads this", None, None, 422),
                                                                                       ])
def update_one_post(authorized_client, uri_id, title, content, published, writer_id, status_code):
    uri = str(uri_id)
    res = authorized_client.put("/posts/"+uri, json={"title":title, "content": content, "published":published, "writer_id":writer_id})
    assert res.status_code == status_code
    
    
@pytest.mark.parametrize("uri_id, status_code", [(15, 404), (20, 404)])
def test_get_delete_one(uri_id, authorized_client, status_code):
    uri = str(uri_id)
    res = authorized_client.delete('/posts/'+ uri)
    assert res.status_code == status_code