import psycopg2
from psycopg2.extras import RealDictCursor # add names of fiels to cursor
from random import randrange
from fastapi import Body, FastAPI, HTTPException, Response, status
from pydantic import BaseModel  # Schema
from typing import Optional  # Optional fields
try:
    db_connection = psycopg2.connect(
        host='localhost',
        database='Reddit Clone',
        user='postgres',
        password="findMepodatke558#",
        cursor_factory=RealDictCursor
    )
    cursor = db_connection.cursor()
    print('Database Connection: successful')
except Exception as e:
    print('Database connection: failed')
    print("Error:", e.json)





class BlogPost(BaseModel):  # Pydantic schema for POST Body validation
    title: str
    content: str
    published: bool = True
    rating: Optional[int] = None
    writer_id : int


##### DATA #####: Array of Blog Posts (Local list)
my_posts = [ 
    {"id": 1, "title": "Welcome to our blog", "content": "This is the beginning..."},
    {"id": 2, "title": "Top 10 best activities in Luxembourg", "content": "Our list of..."}
]

# find and return the post with the a given i
def find_post(given_id):
    for post in my_posts:
        if post["id"] == given_id:
            return post

# Find and return the index of a specific post
def find_post_index(given_id):
    for index, post in enumerate(my_posts):
        if post["id"] == given_id:
            return index

        
###### FastAPI instance name ######
app = FastAPI()  



#######################################
#### "Path Operations" or "Routes"  ###
#######################################

# GET / :
@app.get("/")  # decorator
def hello():  # function
    return {"message": "Hello from my first API"}  # response



# GET /posts ***GET ALL BLOGPOSTS***
@app.get("/posts")
def get_posts():
    # EXecuting the SQL query
    cursor.execute('SELECT * FROM BLOGPOST ORDER BY "ID"')
    # Retrieve all the posts (lists)
    database_posts = cursor.fetchall()
    return {"data": database_posts}



# POST /posts  ***CREATE NEW BLOGPOST***
@app.post("/posts", status_code=status.HTTP_201_CREATED)   # POST /posts endpoint
def create_posts(new_post: BlogPost, response: Response):
    try:# Use the pydantic schema class
                             # Convert the pydantic model to a dictionary                                                 # get last id in the list and increment it by 1
        cursor.execute("INSERT INTO BLOGPOST (title, content, published, writer_id) VALUES (%s ,%s, %s, %s) RETURNING *;",
                    (new_post.title, new_post.content, new_post.published, new_post.writer_id) )
        post_body = cursor.fetchone()
        db_connection.commit()
    except psycopg2.errors.ForeignKeyViolation as e:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"foreign key with writer_id:{new_post.writer_id}")
    # response.status_code = 201                           # Alternative solution to change the HTTP code
    return {"data": post_body}    



#GET /posts/trending  ***GET LATEST BLOGPOST***


#GET /posts/{id_param}  ***GET POST WITH ID***
@app.get('/posts/{id_param}')                              # {id_param}  is the path parameter
def get_post(id_param: int, response: Response): 
    cursor.execute('SELECT * FROM blogpost WHERE "ID" = %s;', (id_param,))# id_param must be an integer                                       
    corresponding_post = cursor.fetchone()               # use the function with converted URI
    if not corresponding_post:                             # If No corresponding post found throw an exception to the consumer
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )
    return {"data": corresponding_post}   



@app.delete('/posts/{id_param}')
def delete_post(id_param: int):
    cursor.execute('DELETE FROM blogpost WHERE "ID" = %s RETURNING *;', (id_param,))
    post_dig = cursor.fetchone()
    db_connection.commit()
    if not post_dig:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )

    # 2. Remove element from array
    return {"data": Response(status_code=status.HTTP_204_NO_CONTENT)}




#PUT /posts/{id_param}  ***REPLACE POST WITH ID***
@app.put('/posts/{id_uri}')
def update_post(id_uri: int, post_body: BlogPost):
    try:
        cursor.execute('UPDATE blogpost SET title=%s, content=%s, writer_id=%s WHERE "ID"=%s RETURNING *;',
                       (post_body.title, post_body.content,post_body.writer_id,str(id_uri)))
        updated_post= cursor.fetchone()
        db_connection.commit()
        if not updated_post:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"Not corresponding post was found with id:{id_uri}")
        return{"data": updated_post}
    except psycopg2.errors.ForeignKeyViolation as err:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,detail=f"Foreign key violation with writer_id:{post_body.writer_id}")
        