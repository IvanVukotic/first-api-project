from datetime import datetime
from pydantic import BaseModel, EmailStr

#These classes make easy use of the data in the tables just don't make typos and give them the same name like in the tables
class UserPy(BaseModel):
    email: EmailStr
    password: str
    
class User_Response(UserPy):
    id: int
    created_at: datetime
    class Config:
        orm_mode = True
        
class BlogPost(BaseModel):
    title: str
    content: str
    writer_id: int
    
class BlogPost_Response(BlogPost):
    id: int
    created_at: datetime
#this will get in the response all the data from the user not just the id
    writer: User_Response
    published : bool 
    class Config:
        orm_mode = True