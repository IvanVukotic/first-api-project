from typing import List
from database import get_db, database_engine
from fastapi import Depends, FastAPI, HTTPException, Response, status
from sqlalchemy.orm import Session
from sqlalchemy import update
import models, schemas

models.Base.metadata.create_all(bind=database_engine)

app = FastAPI()
#get all from users the reponse is a list because it has multiple rows of data
@app.get('/users', response_model=List[schemas.User_Response])
#Session is always needed
def get_users(db: Session = Depends(get_db)):
    #This is like SELECT * FROM user 
    all_users = db.query(models.User).all()
    return all_users
#similar to user
@app.get('/posts', response_model=List[schemas.BlogPost_Response])
def get_blogposts(db: Session = Depends(get_db)):
    all_blogposts = db.query(models.BlogPost).all()
    return all_blogposts
#this is by id and Where id = id_uri is the filter(models.User.id == uri_id) and the .first() is telling it not to querry any further
@app.get('/users/{uri_id}', response_model=schemas.User_Response)
def get_user(uri_id: int, db: Session = Depends(get_db)):
    corresponding_user = db.query(models.User).filter(models.User.id == uri_id).first()
    if not corresponding_user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"Not corresponding user was found with id:{uri_id}")
    return corresponding_user
#similar just for blogpost
@app.get('/posts/{uri_id}', response_model=schemas.BlogPost_Response)
def get_user(uri_id: int, db: Session = Depends(get_db)):
    corresponding_blog = db.query(models.BlogPost).filter(models.BlogPost.id == uri_id).first()
    if not corresponding_blog :
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"Not corresponding user was found with id:{uri_id}")
    return corresponding_blog
#post user_body is what you want to get into the database from postman but first it needs to change into dictionary and ** means get all from it nomatter how long, and add()into the table
@app.post('/users', response_model=schemas.User_Response)
def create_user(user_body: schemas.UserPy, db:Session=Depends(get_db)):
    new_user = models.User(**user_body.dict())
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user
#similar post
@app.post('/posts', response_model=schemas.BlogPost_Response)
def create_post(blogpost_body: schemas.BlogPost, db:Session=Depends(get_db)):
    new_post = models.BlogPost(**blogpost_body.dict())
    db.add(new_post)
    db.commit()
    db.refresh(new_post)
    return new_post
#delete by getting it the same way like get does but when you get it use delete()
@app.delete('/users/{uri_id}')
def delete_user(uri_id: int, db: Session=Depends(get_db)):
    corresponding_deluser = db.query(models.User).filter(models.User.id == uri_id).first()
    if not corresponding_deluser :
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"Not corresponding user was found with id:{uri_id}")
    else:
        db.delete(corresponding_deluser)
        db.commit()
        return corresponding_deluser
#similar just blogpost
@app.delete('/posts/{uri_id}')
def delete_user(uri_id: int, db: Session=Depends(get_db)):
    corresponding_delpost = db.query(models.BlogPost).filter(models.BlogPost.id == uri_id).first()
    if not corresponding_delpost :
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"Not corresponding user was found with id:{uri_id}")
    else:
        db.delete(corresponding_delpost)
        db.commit()
        return corresponding_delpost
#update by getting the the row with the id but try to do .first() in another line, not sure how but this way it works better, and for it the function is update(), synchronize_session is just so if the session gets down it will still commit   
@app.put('/users/{uri_id}', response_model=schemas.User_Response)
def update_user(uri_id: int, user_body:schemas.UserPy, db: Session=Depends(get_db)):
    user_query = db.query(models.User).filter(models.User.id == uri_id)
    user = user_query.first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"Not corresponding user was found with id:{uri_id}")
    else:
        user_query.update(user_body.dict(), synchronize_session=False)
    db.commit()
    return user

@app.put('/posts/{uri_id}', response_model=schemas.BlogPost_Response)
def update_user(uri_id: int, posts_body:schemas.BlogPost, db: Session=Depends(get_db)):
    post_query = db.query(models.BlogPost).filter(models.BlogPost.id == uri_id)
    blogpost = post_query.first()
    if not blogpost:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"Not corresponding post was found with id:{uri_id}")
    
    post_query.update(posts_body.dict(), synchronize_session=False)
    db.commit()
    return blogpost