from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.sql.sqltypes import TIMESTAMP
from sqlalchemy.sql.expression import text
from sqlalchemy.orm import relationship
#here you write what you want your tables to look like 

Base = declarative_base()

#these classes are used to make databases Column(type of data, if necessary is it a key, nullable= can it be null)
class User(Base):
    __tablename__ = "user"
    id = Column(Integer, primary_key = True, nullable=False)
    email = Column(String, nullable= False, unique=True)
    password = Column(String, nullable=False)
#server_default= text('to write functions')
    created_at = Column(TIMESTAMP(timezone=True),server_default=text('now()'), nullable=False)

    
class BlogPost(Base):
    __tablename__ = "blogpost"
    id = Column(Integer, primary_key=True, nullable=False)
    title = Column(String, nullable= False)
    content = Column(String, nullable =False)
    published = Column(Boolean, nullable= False, default=True)
    created_at = Column(TIMESTAMP(timezone=True),server_default=text('now()'), nullable=False)
# this is a way to make a column for the foreign key and ondelete means if the foreign key is deleted in it's own table every data is deleted as well
    writer_id = Column(Integer, ForeignKey('user.id', ondelete='CASCADE'), nullable=False)
    writer = relationship("User")