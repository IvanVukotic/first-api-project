from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
#this goes as typeofdatabase://user on your system:password@where is the database:port/name of database
DATABASE_URL='postgresql://postgres:findMepodatke558#@localhost:5432/sqlalchemy'

#make an engine which will later write on the database
database_engine = create_engine(DATABASE_URL)

# make a session which will hold the communication between the code and the database
SessionTemplate = sessionmaker(autocommit=False, autoflush=False, bind=database_engine)

#just to make that the database always connects
def get_db():
    db = SessionTemplate()
    try:
        yield db
    finally:
        db.close()
        